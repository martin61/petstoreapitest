﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PetStoreAPITest
{
    class Program
    {
        static void Main(string[] args)
        {
            Testcases.CreatePet();
            Testcases.FindPetById();
            Testcases.FindPetByStatus();
            Testcases.UpdatePet();
            Testcases.UpdatePetFormData();
            Testcases.DeletePet();

            Console.WriteLine("Press any key to close program");
            Console.ReadKey();
        }
    }
}
