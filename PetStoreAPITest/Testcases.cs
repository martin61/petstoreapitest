﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PetStoreAPITest
{
    static class Testcases
    {
        private static string testdataId = "334643";
        private static string testdataName = "Katze";
        private static string testdataStatus = "available";
        private static string testdataPhotoUrl = "";
        private static string testdataCategoryId = "0";
        private static string testdataCategoryName = "";
        private static string testdataTagId = "0";
        private static string testdataTagName = "";

        public static void CreatePet()
        {
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);

            RestResponse response = PetStoreRestCalls.PostPet(pet.ToString());

            XDocument responsePet = XDocument.Parse(response.body);
            
            try
            {
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Assert.AreEqual(testdataId, responsePet.Root.Element("id").Value, "Unexpected Id");
                Assert.AreEqual(testdataName, responsePet.Root.Element("name").Value, testdataName, "Unexpected Name");
                Assert.AreEqual(testdataStatus, responsePet.Root.Element("status").Value, testdataStatus, "Unexpected Status");
                Assert.AreEqual(testdataPhotoUrl, responsePet.Root.Element("photoUrls").Element("photoUrl").Value, testdataPhotoUrl, "Unexpected PhotoUrl");
                Assert.AreEqual(testdataCategoryId, responsePet.Root.Element("category").Element("id").Value, testdataCategoryId, "Unexpected Category Id");
                Assert.AreEqual(testdataCategoryName, responsePet.Root.Element("category").Element("name").Value, "Unexpected Category Name");
                Assert.AreEqual(testdataTagId, responsePet.Root.Element("tags").Element("tag").Element("id").Value, "Unexpected Tag Id");
                Assert.AreEqual(testdataTagName, responsePet.Root.Element("tags").Element("tag").Element("name").Value, "Unexpected Tag name");
                Console.WriteLine("Testcase 1: Create Pet succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 1: Create Pet failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
            PetStoreRestCalls.DeletePet(testdataId);
        }

        public static void UpdatePetFormData()
        {
            string newName = "Katze2";
            string newStatus = "sold";
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            PetStoreRestCalls.PostPet(pet.ToString());

            RestResponse response = PetStoreRestCalls.PostPetPetId(testdataId, newName, newStatus);

            try
            {
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Console.WriteLine("Testcase 5: Update Pet with Form Data succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 5: Update Pet with Form Data failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
            PetStoreRestCalls.DeletePet(testdataId);
        }

        public static void UpdatePet()
        {
            string newName = "Katze2";
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            PetStoreRestCalls.PostPet(pet.ToString());
            
            XDocument changedPet = CreateXmlPet(testdataId, newName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            RestResponse response = PetStoreRestCalls.PutPet(changedPet.ToString());
            
            XDocument responsePet = XDocument.Parse(response.body);

            try
            {
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Assert.AreEqual(testdataId, responsePet.Root.Element("id").Value, "Unexpected Id");
                Assert.AreEqual(newName, responsePet.Root.Element("name").Value, testdataName, "Unexpected Name");
                Assert.AreEqual(testdataStatus, responsePet.Root.Element("status").Value, testdataStatus, "Unexpected Status");
                Assert.AreEqual(testdataPhotoUrl, responsePet.Root.Element("photoUrls").Element("photoUrl").Value, testdataPhotoUrl, "Unexpected PhotoUrl");
                Assert.AreEqual(testdataCategoryId, responsePet.Root.Element("category").Element("id").Value, testdataCategoryId, "Unexpected Category Id");
                Assert.AreEqual(testdataCategoryName, responsePet.Root.Element("category").Element("name").Value, "Unexpected Category Name");
                Assert.AreEqual(testdataTagId, responsePet.Root.Element("tags").Element("tag").Element("id").Value, "Unexpected Tag Id");
                Assert.AreEqual(testdataTagName, responsePet.Root.Element("tags").Element("tag").Element("name").Value, "Unexpected Tag name");
                Console.WriteLine("Testcase 4: Update Pet succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 4: Update Pet failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
            PetStoreRestCalls.DeletePet(testdataId);
        }

        public static void FindPetById()
        {
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            PetStoreRestCalls.PostPet(pet.ToString());

            RestResponse response = PetStoreRestCalls.GetPetById(testdataId);

            XDocument responsePet = XDocument.Parse(response.body);

            try
            {
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Assert.AreEqual(testdataId, responsePet.Root.Element("id").Value, "Unexpected Id");
                Assert.AreEqual(testdataName, responsePet.Root.Element("name").Value, testdataName, "Unexpected Name");
                Assert.AreEqual(testdataStatus, responsePet.Root.Element("status").Value, testdataStatus, "Unexpected Status");
                Assert.AreEqual(testdataPhotoUrl, responsePet.Root.Element("photoUrls").Element("photoUrl").Value, testdataPhotoUrl, "Unexpected PhotoUrl");
                Assert.AreEqual(testdataCategoryId, responsePet.Root.Element("category").Element("id").Value, testdataCategoryId, "Unexpected Category Id");
                Assert.AreEqual(testdataCategoryName, responsePet.Root.Element("category").Element("name").Value, "Unexpected Category Name");
                Assert.AreEqual(testdataTagId, responsePet.Root.Element("tags").Element("tag").Element("id").Value, "Unexpected Tag Id");
                Assert.AreEqual(testdataTagName, responsePet.Root.Element("tags").Element("tag").Element("name").Value, "Unexpected Tag name");
                Console.WriteLine("Testcase 2: Find Pet succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 2: Find Pet failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
            PetStoreRestCalls.DeletePet(testdataId);
        }

        public static void FindPetByStatus()
        {
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            PetStoreRestCalls.PostPet(pet.ToString());

            RestResponse response = PetStoreRestCalls.GetPetByStatus(testdataStatus);

            XDocument responsePet = XDocument.Parse(response.body);

            try
            {
                Func<XElement, bool> compareStatus = s => s.Element("status").Value.Equals(testdataStatus);
                Func<XElement, bool> compareId = s => s.Element("id").Value.Equals(testdataId);
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Assert.AreEqual(true, responsePet.Root.Elements("Pet").All(compareStatus), "Response contains Pet with unexpected Status");
                Assert.AreEqual(true, responsePet.Root.Elements("Pet").Any(compareId), "Previously createt Pet missing");
                Console.WriteLine("Testcase 3: Find Pet by Status succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 3: Find Pet by Status failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
            PetStoreRestCalls.DeletePet(testdataId);
        }
        
        public static void DeletePet()
        {
            XDocument pet = CreateXmlPet(testdataId, testdataName, testdataStatus, testdataPhotoUrl, testdataCategoryId, testdataCategoryName, testdataTagId, testdataTagName);
            PetStoreRestCalls.PostPet(pet.ToString());
            
            RestResponse response = PetStoreRestCalls.DeletePet(testdataId);

            try
            {
                Assert.AreEqual(System.Net.HttpStatusCode.OK, response.responseCode, "Unexpected Http Response Code");
                Console.WriteLine("Testcase 6: Delete Pet succeeded");
            }
            catch (Exception e)
            {
                Console.WriteLine("Testcase 6: Delete Pet failed");
                Console.WriteLine("Error Message: " + e.Message);
            }
        }

        private static XDocument CreateXmlPet(string id, string name, string status, string photoUrl, string categoryId, string categoryName, string tagId, string tagName)
        {
            XElement category = new XElement("category",
                                                new XElement("id", categoryId),
                                                new XElement("name", categoryName));
            XElement photoUrls = new XElement("photoUrls",
                                                new XElement("photoUrl", photoUrl));
            XElement tags = new XElement("tags",
                                                new XElement("tag",
                                                    new XElement("id", tagId),
                                                    new XElement("name", tagName)));

            XDocument xmlPet = new XDocument(new XElement("Pet",
                                            new XElement("id", id),
                                            category,
                                            new XElement("name", name),
                                            photoUrls,
                                            tags,
                                            new XElement("status", status)));
            
            return xmlPet;
        }
    }
}
