﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;

namespace PetStoreAPITest
{
    static class PetStoreRestCalls
    {

        private static string baseUri = "https://petstore.swagger.io/v2";

        public static RestResponse PostPet(string pet)
        {
            try
            {
                string uri = baseUri + "/pet";
                
                byte[] data = Encoding.UTF8.GetBytes(pet);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/xml";
                req.Accept = "application/xml";
                req.Method = WebRequestMethods.Http.Post;
                req.ContentLength = data.Length;
                
                Stream datastream = (Stream)req.GetRequestStream();
                datastream.Write(data, 0, data.Length);
                datastream.Close();
                
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }

        public static RestResponse PutPet(string pet)
        {
            try
            {
                string uri = baseUri + "/pet";

                byte[] data = Encoding.UTF8.GetBytes(pet);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/xml";
                req.Accept = "application/xml";
                req.Method = WebRequestMethods.Http.Put;
                req.ContentLength = data.Length;

                Stream datastream = (Stream)req.GetRequestStream();
                datastream.Write(data, 0, data.Length);
                datastream.Close();

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }
        
        public static RestResponse GetPetById(string id)
        {
            try
            {
                string uri = baseUri + "/pet/" + id;
                
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.Accept = "application/xml";
                req.Method = WebRequestMethods.Http.Get;
                
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }

        public static RestResponse GetPetByStatus(string status)
        {
            try
            {
                string uri = baseUri + "/pet/findByStatus" + "?status=" + status;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.Accept = "application/xml";
                req.Method = WebRequestMethods.Http.Get;
                
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }

        public static RestResponse PostPetPetId(string id, string name, string status)
        {
            try
            {
                string uri = baseUri + "/pet/" + id;
                
                byte[] data = Encoding.UTF8.GetBytes("name=" + name + "&status=" + status);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/x-www-form-urlencoded";
                req.Accept = "application/xml";
                req.Method = WebRequestMethods.Http.Post;
                req.ContentLength = data.Length;

                Stream datastream = (Stream)req.GetRequestStream();
                datastream.Write(data, 0, data.Length);
                datastream.Close();

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }

        public static RestResponse DeletePet(string id)
        {
            try
            {
                string uri = baseUri + "/pet/" + id;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.Accept = "application/xml";
                req.Method = "DELETE";

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseBody = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return new RestResponse(responseBody, response.StatusCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new RestResponse();
            }
        }
    }
}
