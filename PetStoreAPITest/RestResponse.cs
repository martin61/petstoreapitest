﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace PetStoreAPITest
{
    class RestResponse
    {
        public string body;
        public HttpStatusCode responseCode;

        public RestResponse()
        {
            
        }

        public RestResponse(string body, HttpStatusCode responseCode)
        {
            this.body = body;
            this.responseCode = responseCode;
        }
        
    }
}
